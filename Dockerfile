FROM fedora:33

ARG uid
ARG branch=master
ENV branch=$branch

RUN dnf update -y \
 && dnf install -y \
      git \
      make \
      cmake \
      which \
      mingw32-libxml2-static \
      mingw32-filesystem \
      mingw64-filesystem \
      mingw32-freetype \
      mingw32-freetype-static \
      mingw64-freetype \
      mingw64-freetype-static \
      mingw32-gcc-c++ \
      mingw64-gcc-c++ \
      mingw32-libgomp \
      mingw64-libgomp \
      mingw32-pkg-config \
      mingw64-pkg-config \
      mingw32-qt5-qtbase \
      mingw32-qt5-qtbase-static \
      mingw64-qt5-qtbase \
      mingw64-qt5-qtbase-static \
      mingw32-SDL2 \
      mingw64-SDL2 \
      mingw32-libxml2 \
      mingw32-libxml2-static \
      mingw64-libxml2 \
      mingw64-libxml2-static \
      mingw64-zlib-static \
      mingw32-zlib \
      mingw32-zlib-static \
      mingw64-zlib \
      mingw64-zlib-static \
 && rm -rf /var/cache/dnf/*

RUN useradd developer && \
    usermod -aG wheel developer

ENV HOME=/home/developer

USER developer
WORKDIR /home/developer
VOLUME /home/developer

CMD /bin/bash
